# README #

# 500Tech React & Redux RSS Feed assignment

## Overview

The goal is to create a simple RSS feed display.

The left side allows selecting, adding and removing RSS feeds.

The right side displays the currently selected RSS feed.

## Wireframes
![Alt text](https://s3.amazonaws.com/500tech-shared/angular+home+assignment+500tech+wireframes.png)

## Definitions

### Main screen
1. Header - showing the URL of the feed
2. Content - showing the feed items
<br>
The feed items themselves should be written as a component that displays the title, date, and body.

### Sidebar
1. String input - RSS URL + submit button
2. URL history list - list of URLs the user viewed

* When submitting an RSS URL, its URL should be inserted to the top of the history list in an “active” (selected) state, and the main section should display its feed.
* When clicking on an item (URL) from that list, it should get an “active” (selected) state, and the main section should display its feed.
* When hitting browser back button, it should navigate back to the previous URL that was active
* Each history item should also contain an “x” button, to remove that item from the list.
* The list items should be persistent, should stay on page refresh.

## RSS feed API
Google JSON Feed API:<br>

    http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&q=[URL]

Sample feeds:
* http://www.feedforall.com/sample.xml
* http://www.feedforall.com/sample-feed.xml
* http://www.feedforall.com/blog-feed.xml
* http://www.rss-specifications.com/blog-feed.xml


## Tools
* ES6
* Webpack & Babel
* twitter bootstrap (not manadatory)

## Submission

Please send your result as a Github/Bitbucket repository with clear instructions on how to setup and run.
Feel free to add text for describing the decisions you've made along the way.

Best of Luck !

500Tech Team


## Live Server
Yon see the app up and running live from here: https://goo.gl/xVqPQu




## How to install the App on your local laptop

### With Docker:
1. Install the docker client for your machine if not installed previously. Instructions: https://docs.docker.com/install/
2. Copy the Dockerfile (https://goo.gl/VpTvPu) in your desired folder.
3. Open a terminal on that folder and build the glint_test image running this code (this might take a while):
```
docker build -t glint_test .
```
4. Run a container out from the previous image running this code:
```
docker run -p 8083:8083 --rm -i --name glint_test glint_test
```
5. You can access the web going to http://localhost:8083/ on your browser
6. To stop the container running you have to open a new terminal and run this code:
```
docker stop glint_test
```

### Without Docker:
1. Install nvm if is not installed on your computer. Instructions: https://github.com/creationix/nvm
2. Set the node version to v9.3.0 in nvm. Instructions: https://www.sitepoint.com/quick-tip-multiple-versions-node-nvm/
3. Install pm2 globally if is not installed with:
```
npm install pm2 -g
```
4. Install git if not installed. Instructions here: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
5. Clone the repository to the desired folder: https://bitbucket.org/txabox/test3497987
6. Install all the dependencies with:
```
npm install
```
7. Package all the files with webpack like this (choose whether you want the App to be run in 'dev' or 'prod' environment):
```
npm run start:front:[dev|prod]
```
8. Run the express server (running in port 8083) (choose whether you want the App to be run in 'dev' or 'prod' environment):
```
npm run start:back:[dev|prod]
```
9. You can access the web going to http://localhost:8083/ on your browser
