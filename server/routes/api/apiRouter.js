import express from 'express'
import axios from 'axios'

const router = express.Router()

router
  .get('/feed', (req, res) => {
    axios.get(` https://api.rss2json.com/v1/api.json?rss_url=${req.query.url}`)
      .then((response) => {
        res.status(response.status).json(response.data)
      })
      .catch((error) => {
        console.error('Error retrieving the json: ', error)
        res.status(error.response.status).json(error.response.data)
      })
  })

export default router
