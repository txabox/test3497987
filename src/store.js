import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux'
import thunk from 'redux-thunk'

import indexReducer from './reducers/index/indexReducer'
import urlReducer from './reducers/url/urlReducer'
import itemsCollectionReducer from './reducers/itemsCollection/itemsCollectionReducer'
import searcherReducer from './reducers/searcher/searcherReducer'

const reducers = combineReducers({
  index: indexReducer,
  url: urlReducer,
  itemsCollection: itemsCollectionReducer,
  searcher: searcherReducer
})

export default createStore(
  reducers,
  applyMiddleware(thunk))
