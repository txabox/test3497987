import React from 'react'
import PropTypes from 'prop-types'
import Measure from 'react-measure'
import styled from 'styled-components'

import Sidebar from './components/Sidebar/Sidebar'
import MainScreen from './components/MainScreen/MainScreen'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as IndexActionCreators from './IndexActionCreators'

const StyledIndex = styled.div`
  background-color: #fff;
  height:100%;
  margin:0;
  padding: 20px 10px;
  display: flex;
  flex-direction: column;
  .two-cols-container {
      flex:2;
      display: flex;
  }
`

const Index = ({history, match, actions}) => (
  <Measure
    bounds
    onResize={(contentRect) => {
      actions.updateDimensions(contentRect.bounds)
    }}
  >
    {({ measureRef }) =>
      <StyledIndex innerRef={measureRef}>
        <div className="two-cols-container">
          <Sidebar match={match} history={history} />
          <MainScreen match={match} />
        </div>
      </StyledIndex>
    }
  </Measure>
)

Index.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    indexDimensions: state.index.dimensions
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(IndexActionCreators, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)
