export const updateDimensions = (indexDimensions) => {
  return {
    type: 'UPDATE_INDEX_DIMENSIONS',
    payload: {
      indexDimensions
    }
  }
}
