import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { connect } from 'react-redux'

import Title from './components/Title/Title'
import ItemsCollection from './components/ItemsCollection/ItemsCollection'

const StyledMainScreen = styled.div`
  flex: 1 1 70%;
  margin-left: 20px;
  padding: 10px 10px 50px 10px;
  overflow: hidden;
  height: ${(props) => props.indexDimensions.height - 30}px;
`

const MainScreen = ({match, indexDimensions}) => (
  <StyledMainScreen indexDimensions={indexDimensions}>
    <Title match={match} />
    <ItemsCollection match={match} />
  </StyledMainScreen>
)

MainScreen.propTypes = {
  match: PropTypes.object.isRequired,
  indexDimensions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    indexDimensions: state.index.dimensions
  }
}

export default connect(mapStateToProps)(MainScreen)
