import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledTitle = styled.h3`
  margin-top: 0;
  margin-bottom: 30px
`

const Title = ({match}) => (
  <StyledTitle>
    { match.params.id
      ? <span>{decodeURIComponent(match.params.id)} </span>
      : <span>Welcome</span>
    }
  </StyledTitle>
)

Title.propTypes = {
  match: PropTypes.object.isRequired
}

export default Title
