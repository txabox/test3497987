import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as ItemsCollectionActionCreators from './ItemsCollectionActionCreators'
import { isUrlInCollection } from 'Utils/utils'

import Item from './components/Item/Item'

const StyledItemsCollection = styled.div`
  overflow-y: scroll;
  height: 100%;
  ul{
    list-style: none;
    padding: 0;
  }
`

class ItemsCollection extends React.Component {
  componentDidMount() {
    // create timers, listeners...
    // load items the 1st time
    this._loadItemsFromURL(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    const nextURLCollection = nextProps.urlCollection
    const nextURL = nextProps.match.params.id

    // Do not load because it is a url that is not in the collection anymore
    if (nextURL && !isUrlInCollection(nextURL, nextURLCollection)) { return false }

    // 1. Check if curUrl has changed
    if (nextURL !== this.props.match.params.id) {
      this._loadItemsFromURL(nextURL)
      return true
    }
  }

  _loadItemsFromURL = (feedURL) => {
    if (!feedURL) return

    this.props.actions.setIsLoading(true)

    fetch('/api/feed?url=' + feedURL)
      .then(res => res.json())
      .then(data => {
        if (data.status === 'error') {
          alert(`Error retrieving items for feed ${feedURL}. Error: ${data.message}`)
          console.error(`Error retrieving items for feed ${feedURL}`, data)
          return
        }
        const items = data.items
        this.props.actions.updateItems(items)
        this.props.actions.setIsLoading(false)
      })
      .catch((error) => {
        if (error) {
          alert(`Error retrieving items for feed ${feedURL}`)
          console.error(`Error retrieving items for feed ${feedURL}`, error)
        }
      })
  }

  render() {
    return <StyledItemsCollection>
      {
        this.props.isLoading
        ? <div>Loading feed...</div>
        : this.props.match.params.id
          ? <ul>
            {
              this.props.items.map((item) =>
                <Item key={item.guid} data={item} />
              )
            }
          </ul>
          : <div>
            Please, input a propper URL to retrieve the feeds.
          </div>

      }

    </StyledItemsCollection>
  }
}

ItemsCollection.propTypes = {
  match: PropTypes.object.isRequired,
  items: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired,
  urlCollection: PropTypes.array.isRequired
}

const mapStateToProps = (state) => {
  return {
    items: state.itemsCollection.items,
    isLoading: state.itemsCollection.isLoading,
    urlCollection: state.url.collection
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(ItemsCollectionActionCreators, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsCollection)
