export const updateItems = (items) => {
  return {
    type: 'UPDATE_ITEMS_COLLECTION',
    payload: {
      items
    }
  }
}

export const setIsLoading = (isLoading) => {
  return {
    type: 'ITEM_COLLECTION_IS_LOADING',
    payload: {
      isLoading
    }
  }
}
