import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Panel } from 'react-bootstrap'

const StyledItem = styled.li`
  margin-bottom: 10px;
`

const Item = ({data}) => (
  <StyledItem>
    <Panel bsStyle="success">
      <Panel.Heading>
        <Panel.Title componentClass="h3">{data.title} - {data.pubDate}</Panel.Title>
      </Panel.Heading>
      <Panel.Body dangerouslySetInnerHTML={{__html: data.content}}></Panel.Body>
    </Panel>
  </StyledItem>
)

Item.propTypes = {
  data: PropTypes.object.isRequired
}
export default Item
