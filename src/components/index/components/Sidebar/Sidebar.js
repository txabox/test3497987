import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { connect } from 'react-redux'

import Searcher from './components/Searcher/Searcher'
import UrlsCollection from './components/UrlsCollection/UrlsCollection'

const StyledSidebar = styled.div`
  flex :0 0 30%;
  padding: 10px 10px 100px 10px;
  overflow: hidden;
  height: ${(props) => props.indexDimensions.height - 30}px;
`

const Sidebar = ({history, match, indexDimensions}) => (
  <StyledSidebar indexDimensions={indexDimensions}>
    <Searcher history={history} />
    <UrlsCollection match={match} history={history} />
  </StyledSidebar>
)

Sidebar.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  indexDimensions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    indexDimensions: state.index.dimensions
  }
}

export default connect(mapStateToProps)(Sidebar)
