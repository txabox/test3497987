import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Button } from 'react-bootstrap'

const StyledUrl = styled.li`

  margin-bottom: 10px;

  button{
    white-space: normal;
    overflow-wrap: break-word;
  }
  .url-string{
    width: 90%;
    margin-right: 2%
  }
  .remove-url{
    width: 8%
  }
`

const Url = ({url, isActive, removeURL}) => (
  <StyledUrl>
    <Link to={`/${encodeURIComponent(url)}`}>
      <Button
        bsClass={'btn url-string ' + (isActive ? 'btn-success' : 'btn-default')}
        active={isActive}>
        {url}
      </Button>
    </Link>
    <Button
      bsClass="btn btn-xs btn-danger remove-url"
      onClick={(e) => removeURL(url)} >
    x
    </Button>
  </StyledUrl>
)

Url.propTypes = {
  url: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
  removeURL: PropTypes.func.isRequired
}

export default Url
