import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import storageStore from 'store2'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as UrlsCollectionActionCreators from './UrlsCollectionActionCreators'
import { isUrlInCollection } from 'Utils/utils'

import Url from './components/Url/Url'

const StyledUrlsCollection = styled.div`
  overflow-y: scroll;
  height: 100%;
  ul{
    list-style: none;
    padding: 0;
  }
`
class UrlsCollection extends React.Component {
  componentDidMount() {
    // create timers, listeners...
    // on 1st load set store urlCollection from local/session storage:
    const urlCollection = storageStore.get('urlCollection') ? storageStore.get('urlCollection') : []
    if (urlCollection.length === 0) {
      this.props.history.replace('/')
    }
    this.props.actions.setInitialUrlCollection(urlCollection)
  }

  componentWillReceiveProps(nextProps) {
    const nextURLCollection = nextProps.urlCollection
    const nextURL = nextProps.match.params.id

    // 1. Check if urlCollections has change:
    if (this.props.urlCollection.length !== nextURLCollection.length) {
      storageStore.set('urlCollection', nextURLCollection)
      this._redirectToUrlIfCurrentIsDeleted(nextURLCollection, nextURL)
      // there is no need to keep on with the rest of the method
      return true
    }

    // 2. Check if curUrl has changed
    if (this.props.match.params.id !== nextURL) {
      this._redirectToUrlIfCurrentIsDeleted(nextURLCollection, nextURL)
    }
  }

  _redirectToUrlIfCurrentIsDeleted = (urlCollection, url) => {
    // check only if a there is a url on the url query string
    if (url) {
      if (urlCollection.length === 0) {
        this.props.history.replace('/')
      } else if (!isUrlInCollection(url, urlCollection)) {
        this.props.history.replace('/' + encodeURIComponent(urlCollection[0]))
      }
    }
  }

  render() {
    return <StyledUrlsCollection>
      <ul>
        {
          this.props.urlCollection.map((url, index) => <Url
            key={url + '_' + index}
            url={url}
            isActive={Boolean(this.props.match.params.id) && this.props.match.params.id === encodeURIComponent(url)}
            removeURL={this.props.actions.removeURL} />
          )
        }
      </ul>
    </StyledUrlsCollection>
  }
}

UrlsCollection.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  urlCollection: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    urlCollection: state.url.collection
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(UrlsCollectionActionCreators, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UrlsCollection)
