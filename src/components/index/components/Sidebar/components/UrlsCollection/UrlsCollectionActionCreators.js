export const setInitialUrlCollection = (collection) => {
  return {
    type: 'SET_INITIAL_URL_COLLECTION',
    payload: {
      collection
    }
  }
}

export const removeURL = (url) => {
  return {
    type: 'REMOVE_URL',
    payload: {
      url
    }
  }
}
