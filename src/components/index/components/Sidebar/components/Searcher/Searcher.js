import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { InputGroup, FormControl, Button, Glyphicon } from 'react-bootstrap'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as SearcherActionCreators from './SearcherActionCreators'

const StyledSearcher = styled.div`
  background-color: white;
  margin-bottom: 20px;
`
class Searcher extends React.Component {
  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.handleClick()
    }
  }

  handleURLChanged = (e) => {
    this.props.actions.updateURL(e.target.value)
  }

  handleClick = (e) => {
    const newUrl = this.props.url
    if (newUrl !== '') {
      // check if current url already existed
      const alreadyExisted = (newUrl) => {
        const arr = this.props.urlCollection
        return arr.indexOf(newUrl) !== -1
      }

      this.props.actions.updateURL('')
      if (alreadyExisted(newUrl)) {
        // if the new url already in url collection, do not add it again
        // simply redirect it to the proper link
        this.props.history.replace(encodeURIComponent(newUrl))
      } else {
        this.props.actions.addNewURL(newUrl)
        this.props.history.push(encodeURIComponent(newUrl))
      }
    }
  }

  render() {
    return <StyledSearcher>
      <InputGroup>
        <FormControl type="text" placeholder="Input URL" value={this.props.url} onChange={this.handleURLChanged} onKeyPress={this.handleKeyPress} />
        <InputGroup.Button>
          <Button onClick={this.handleClick}>
            <Glyphicon glyph="search" />
          </Button>
        </InputGroup.Button>
      </InputGroup>
    </StyledSearcher>
  }
}

Searcher.propTypes = {
  history: PropTypes.object.isRequired,
  urlCollection: PropTypes.array.isRequired,
  url: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    urlCollection: state.url.collection,
    url: state.searcher.url
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(SearcherActionCreators, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Searcher)
