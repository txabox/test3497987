export const addNewURL = (newURL) => {
  return {
    type: 'ADD_NEW_URL',
    payload: {
      newURL
    }
  }
}

export const updateURL = (url) => {
  return {
    type: 'UPDATE_SEARCHER_URL',
    payload: {
      url
    }
  }
}
