export const isUrlInCollection = (url, urlCollection) => {
  return urlCollection.indexOf(decodeURIComponent(url)) !== -1
}
