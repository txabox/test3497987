const defaultValues = {
  url: ''
}

const indexReducer = (state = defaultValues, action) => {
  const newState = {...state}

  if (action.type === 'UPDATE_SEARCHER_URL') {
    newState.url = action.payload.url
  }
  return newState
}

export default indexReducer
