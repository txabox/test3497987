const defaultValues = {
  collection: []
}

function immutableDelete(arr, index) {
  return arr.slice(0, index).concat(arr.slice(index + 1))
}

const urlReducer = (state = defaultValues, action) => {
  const newState = {...state}

  if (action.type === 'SET_INITIAL_URL_COLLECTION') {
    newState.collection = action.payload.collection
  } else if (action.type === 'ADD_NEW_URL') {
    // change the array in an immutable way
    newState.collection = [ action.payload.newURL, ...newState.collection ]
  } else if (action.type === 'REMOVE_URL') {
    // change the array in an immutable way
    newState.collection = immutableDelete(newState.collection, newState.collection.indexOf(action.payload.url))
  }

  return newState
}

export default urlReducer
