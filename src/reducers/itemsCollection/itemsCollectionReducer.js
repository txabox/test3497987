const defaultValues = {
  items: [],
  isLoading: false
}

const itemsCollectionReducer = (state = defaultValues, action) => {
  const newState = {...state}

  if (action.type === 'UPDATE_ITEMS_COLLECTION') {
    newState.items = action.payload.items
  } else if (action.type === 'ITEM_COLLECTION_IS_LOADING') {
    newState.isLoading = action.payload.isLoading
  }
  return newState
}

export default itemsCollectionReducer
