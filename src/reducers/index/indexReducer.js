const defaultValues = {
  dimensions: {
    bottom: 0,
    height: 0,
    left: 0,
    right: 0,
    top: 0,
    width: 0
  }
}

const indexReducer = (state = defaultValues, action) => {
  const newState = {...state}

  if (action.type === 'UPDATE_INDEX_DIMENSIONS') {
    newState.dimensions = action.payload.indexDimensions
  }
  return newState
}

export default indexReducer
