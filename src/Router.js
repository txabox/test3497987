import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Index from './components/index/Index'

class Router extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact={true} path="/" name="index" component={Index} />
          <Route path="/:id?" name="indexWithId" component={Index} />
        </Switch>
      </BrowserRouter>
    )
  }
}

export default Router
